package com.example.indrarachman.ankolayout.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.indrarachman.ankolayout.R
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainView: MainActivityView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainView = MainActivityView()
        MainActivityView().setContentView(this)
    }

    fun showToast(username: String) {
        toast("halo bro $username anda sukses login")
    }
}



