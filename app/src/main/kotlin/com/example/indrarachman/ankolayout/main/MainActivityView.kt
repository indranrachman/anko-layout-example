package com.example.indrarachman.ankolayout.main

import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import com.example.indrarachman.ankolayout.R
import org.jetbrains.anko.*

class MainActivityView : AnkoComponent<MainActivity> {

    private lateinit var mainView: AnkoContext<MainActivity>
    private lateinit var progressBar: ProgressBar

    private val customStyle = { v: Any ->
        when (v) {
            is Button -> v.textSize = 26f
            is EditText -> v.textSize = 24f
        }
    }

    override fun createView(ui: AnkoContext<MainActivity>): View {
        mainView = ui
        with(ui) {
            relativeLayout {

                verticalLayout {
                    lparams(width = matchParent, height = matchParent)

                    val username = editText {
                        hintResource = R.string.app_name
                    }.lparams(width = matchParent, height = wrapContent) {
                        leftMargin = dip(20)
                        rightMargin = dip(20)
                    }

                    val password = editText {
                        hintResource = R.string.app_name
                    }.lparams(width = matchParent, height = wrapContent) {
                        horizontalMargin = dip(20)
                    }

                    button {
                        text = ctx.getString(R.string.app_name)
                        setOnClickListener {
                            owner.showToast(username.text.toString())
                            if (username.text.isEmpty()) progressBar.show() else progressBar.hide()
                            username.setText("")
                            password.setText("")
                        }
                    }
                }.applyRecursively(customStyle)

                progressBar = progressBar {
                    visibility = View.GONE
                }.lparams(width = matchParent, height = wrapContent) { centerInParent() }

            }
        }
        return ui.view
    }

    fun ProgressBar.show() {
        this.visibility = View.VISIBLE
    }

    fun ProgressBar.hide() {
        this.visibility = View.GONE
    }

}